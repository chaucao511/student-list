import './App.css';
import { BrowserRouter, Route, Routes } from 'react-router-dom';
import Stulisting from './components/Stulisting';
import Stucreate from './components/Stucreate';
import Stuedit from './components/Stuedit';
function App() {
  return (
    <div className="App">
      <header className="App-header">
        <h1>
          Einstein Highschool Students
        </h1>
        <BrowserRouter>
          <Routes>
            <Route path='/' element={<Stulisting />}>
            </Route>
            <Route path='/students/create' element={<Stucreate />}>
            </Route>
            <Route path='/students/edit/:stuid' element={<Stuedit />}>
            </Route>

          </Routes>
        </BrowserRouter>
      </header>

    </div>
  );

}

export default App;
