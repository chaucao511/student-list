import { useState } from "react";
import { Link, useNavigate } from "react-router-dom"
const Stucreate = () => {
    const [id, setId] = useState("");
    const [name, setName] = useState("");
    const [classid, setClassid] = useState("");
    const [phoneNumber, setPhoneNumber] = useState("");
    const [email, setEmail] = useState("");
    const [address, setAddress] = useState("");
    const [tbmEnglish, setTbmEnglish] = useState("");
    const navigate = useNavigate();
    // const cleardata = () => {
    //     setId("");
    //     setName("");
    //     setClassid("");
    //     setPhoneNumber("");
    //     setEmail("");
    //     setAddress("");
    //     setTbmEnglish("");
    // }
    const handlesubmit = (e) => {
        e.preventDefault();
        const studata = { id, name, classid, phoneNumber, email, address, tbmEnglish };
        console.log(id, name, classid, phoneNumber, email, address, tbmEnglish);
        fetch("https://6440a14c792fe886a892a617.mockapi.io/stdsdata", {
            method: "POST",
            headers: { "content-type": "application/json" },
            body: JSON.stringify(studata)

        }).then((res) => {
            alert('Save successfully!');
            navigate('/');


        }
        ).catch((err) => {
            console.log(err.message)
        })
    }
    return (
        <div>
            <div className="row">
                <div className="offset-lg-3 col-lg-6">
                    <form className="container" onSubmit={handlesubmit}>
                        <div className="card" style={{ "textAlign": "left" }}>
                            <div className="card-title">
                                <h2>Create new student</h2>
                            </div>
                            <div className="card-body">
                                <div className="row">
                                    <div className="col-lg-12">
                                        <div className="form-group">
                                            <label>ID</label>
                                            <input value={id} disabled="disabled" className="form-control"></input>

                                        </div>
                                    </div>
                                    <div className="col-lg-12">
                                        <div className="form-group">
                                            <label>Name</label>
                                            <input required value={name} onChange={e => setName(e.target.value)} className="form-control"></input>
                                            {name.length == 0 && <span className="text-danger">Enter the name</span>}

                                        </div>
                                    </div>
                                    <div className="col-lg-12">
                                        <div className="form-group">
                                            <label>Class</label>
                                            <input required value={classid} onChange={e => setClassid(e.target.value)} className="form-control"></input>

                                        </div>
                                    </div>
                                    <div className="col-lg-12">
                                        <div className="form-group">
                                            <label>Phone Number</label>
                                            <input required value={phoneNumber} onChange={e => setPhoneNumber(e.target.value)} className="form-control"></input>

                                        </div>
                                    </div>
                                    <div className="col-lg-12">
                                        <div className="form-group">
                                            <label>Email</label>
                                            <input required value={email} onChange={e => setEmail(e.target.value)} className="form-control"></input>

                                        </div>
                                    </div>
                                    <div className="col-lg-12">
                                        <div className="form-group">
                                            <label>Address</label>
                                            <input required value={address} onChange={e => setAddress(e.target.value)} className="form-control"></input>

                                        </div>
                                    </div>
                                    <div className="col-lg-12">
                                        <div className="form-group">
                                            <label>TBM English</label>
                                            <input required value={tbmEnglish} onChange={e => setTbmEnglish(e.target.value)} className="form-control"></input>

                                        </div>
                                    </div>
                                    <div className="col-lg-12">
                                        <div className="form-group">
                                            <button className="btn btn-success" type="submit" > Save </button>
                                            <Link to="/" className="btn btn-danger">Back</Link>
                                        </div>
                                    </div>


                                </div>

                            </div>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    )
}

export default Stucreate;