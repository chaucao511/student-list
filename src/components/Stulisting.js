import { useEffect, useState } from "react";
import { Link, useNavigate } from "react-router-dom";
const Stulisting = () => {

    const [studata, setStudata] = useState();
    const navigate = useNavigate();

    const LoadEdit = (id) => {
        navigate("/students/edit/" + id)

    };
    const RemoveFunction = (id) => {
        if (window.confirm("do you really want to remove?")) {
            fetch("https://6440a14c792fe886a892a617.mockapi.io/stdsdata/" + id, {
                method: "DELETE",


            }).then((res) => {
                alert(' Remove successfully!');
                window.location.reload();


            }
            ).catch((err) => {
                console.log(err.message)
            })
        }

    };

    console.log(studata);
    useEffect(() => {
        fetch("https://6440a14c792fe886a892a617.mockapi.io/stdsdata/").then((res) => {
            return res.json();
        }
        ).then((resp) => {
            setStudata(resp);
            console.log(resp);
        }
        ).catch((err) => {
            console.log(err.message);
        }
        )
    }, []

    )


    return (
        <div className="container">
            <div className="card">
                <div className="card-title">
                    <h2>Student information</h2>

                </div>
                <div className="card-body">
                    <div className="divbtn">
                        <Link to="/students/create" className="btn btn-success">New Student</Link>
                    </div>
                    <table className="table table-bordered">
                        <thead className="bg-dark text-white">
                            <tr>
                                <td>ID</td>
                                <td>Name</td>
                                <td>Class</td>
                                <td>Phone number</td>
                                <td>Email</td>
                                <td>Address</td>
                                <td>TBM English</td>
                                <td>Action</td>
                            </tr>

                        </thead>
                        <tbody>
                            {studata &&
                                studata.map(item => {
                                    return (
                                        <tr key={item.id}>
                                            <td>{item.id}</td>
                                            <td>{item.name}</td>
                                            <td>{item.classid}</td>
                                            <td>{item.phoneNumber}</td>
                                            <td>{item.email}</td>
                                            <td>{item.address}</td>
                                            <td>{item.tbmEnglish}</td>
                                            <td style={{ "display": "flex" }}>
                                                <a onClick={() => { LoadEdit(item.id) }} className="btn btn-success">Edit</a>
                                                <a onClick={() => { RemoveFunction(item.id) }} className="btn btn-danger">Remove</a>
                                                {/* <a className="btn btn-primary">Details</a> */}
                                            </td>
                                        </tr>
                                    )
                                }
                                )}


                        </tbody>
                    </table>

                </div>



            </div>

        </div>

    )
}

export default Stulisting;